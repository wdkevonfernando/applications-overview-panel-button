<!-- README.md -->
<!-- SPDX-License-Identifier: AGPL-3.0-or-later -->

# Applications Overview Panel Button
This extension adds a new button to the panel which will directly open the applications overview. <br>
![Demo of extension](docs/demo.gif)

## Building
### Requirements
- meson >= v0.60.0
- GNOME Shell >= v40

### Instructions
- Download the source and then: <br>
```bash
meson build --prefix $HOME/.local/
meson install -C build
```
- Log out and then log in or restart the session (X11)
- Enable the extension via the extensions app or terminal: <br>
`gnome-extensions enable applications-overview-panel-button@wdkevonfernando.gitlab.com`

## Packaging
```bash
meson build --prefix / -D pack=true
meson install -C build --destdir .
gnome-extensions pack build/applications-overview-panel-button@wdkevonfernando.gitlab.com/ -f -o .
```

## Note
I only created this extension to make opening the application overview with the mouse more convenient for me. 
This means that the plan is just to maintain this extension. So please understand if I am not willing to develop 
new features or if it is not immediately compatible with the newest version of GNOME. 
But feel free either fork this project or open a merge request with your improvements. 

## Acknowledgments
- Florian Müller (fmueller) for Places Status Indicator
- GNOME and contributers for GNOME Shell

## License
This project is distributed under the terms of the GNU Affero General Public License Version 3 or later 
(AGPL3-or-later). See the `LICENSE` file for details.
