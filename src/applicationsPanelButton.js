// applicationsPanelButton.js
// SPDX-License-Identifier: AGPL-3.0-or-later

// Imports
const { Atk, Clutter, GLib, GObject, St } = imports.gi;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const OverviewControls = imports.ui.overviewControls;


// Variables
const BUTTON_DND_ACTIVATION_TIMEOUT = 250;


// Code
var ApplicationsPanelButton = GObject.registerClass(
class ApplicationsPanelButton extends PanelMenu.Button {
    // Create panel button
    _init() {
        super._init(0.0, null, true);
        this.accessible_role = Atk.Role.TOGGLE_BUTTON;

        this.name = 'panelApplications';

        this._label = new St.Label({
            text: _('Applications'),
            y_align: Clutter.ActorAlign.CENTER,
        });
        this.add_actor(this._label);

        this.label_actor = this._label;

        Main.overview.dash.showAppsButton.connect('notify::checked',
            this._onPanelApplicationsToggled.bind(this));


        this._xdndTimeOut = 0;
    }

    // Change style depending on shown view
    _onPanelApplicationsToggled() {
        if (Main.overview.dash.showAppsButton.checked) {
            this.add_style_pseudo_class('overview');
            this.add_accessible_state(Atk.StateType.CHECKED);
        } else {
            this.remove_style_pseudo_class('overview');
            this.remove_accessible_state(Atk.StateType.CHECKED);
        }
    }

    // Toggle between applications overview and other views
    toggleView() {
        if (Main.overview.dash.showAppsButton.checked) { // Applications overview
            Main.overview.hide();
        } else if (Main.overview.visible) { // Activities overview
            // Main.overview.showApps() does not work because Main.overview._shown === true
            Main.overview.dash.showAppsButton.checked = true;
        } else { // Desktop view
            Main.overview.showApps();
        }
    }

    // Handle button press, does some magic I do not understand :]
    handleDragOver(source, _actor, _x, _y, _time) {
        if (source != Main.xdndHandler)
            return DND.DragMotionResult.CONTINUE;

        if (this._xdndTimeOut != 0)
            GLib.source_remove(this._xdndTimeOut);
        this._xdndTimeOut = GLib.timeout_add(GLib.PRIORITY_DEFAULT, BUTTON_DND_ACTIVATION_TIMEOUT, () => {
            this._xdndToggleOverview();
        });
        GLib.Source.set_name_by_id(this._xdndTimeOut, '[gnome-shell] this._xdndToggleOverview');

        return DND.DragMotionResult.CONTINUE;
    }

    vfunc_captured_event(event) {
        if (event.type() == Clutter.EventType.BUTTON_PRESS ||
            event.type() == Clutter.EventType.TOUCH_BEGIN) {
            if (!Main.overview.shouldToggleByCornerOrButton())
                return Clutter.EVENT_STOP;
        }
        return Clutter.EVENT_PROPAGATE;
    }

    vfunc_event(event) {
        if (event.type() == Clutter.EventType.TOUCH_END ||
            event.type() == Clutter.EventType.BUTTON_RELEASE) {
            if (Main.overview.shouldToggleByCornerOrButton())
                this.toggleView();
        }

        return Clutter.EVENT_PROPAGATE;
    }

    vfunc_key_release_event(keyEvent) {
        let symbol = keyEvent.keyval;
        if (symbol == Clutter.KEY_Return || symbol == Clutter.KEY_space) {
            if (Main.overview.shouldToggleByCornerOrButton()) {
                this.toggleView();
                return Clutter.EVENT_STOP;
            }
        }

        return Clutter.EVENT_PROPAGATE;
    }

    _xdndToggleOverview() {
        let [x, y] = global.get_pointer();
        let pickedActor = global.stage.get_actor_at_pos(Clutter.PickMode.REACTIVE, x, y);

        if (pickedActor == this && Main.overview.shouldToggleByCornerOrButton())
            this.toggleView();

        GLib.source_remove(this._xdndTimeOut);
        this._xdndTimeOut = 0;
        return GLib.SOURCE_REMOVE;
    }
});

