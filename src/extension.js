// extension.js
// SPDX-License-Identifier: AGPL-3.0-or-later

// Imports
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;

const { ApplicationsPanelButton } = Me.imports.applicationsPanelButton;


// Code
class Extension {
  constructor() {
    this._panelButton = null;
  }

  enable() {
    log(`Enabling ${Me.metadata.name}`);

    // Add 'show applications' button to panel
    this._panelButton = new ApplicationsPanelButton();
    let position = Main.sessionMode.panel.left.indexOf('activities') + 1;
    Main.panel.addToStatusArea('panelApplications', this._panelButton, position, "left");
  }

  disable() {
    log(`Disabling ${Me.metadata.name}`);

    // Remove 'show applications' button to panel
    this._panelButton.destroy();
    this._panelButton = null;
  }
}

/**
 * @param {ExtensionMeta} meta - An extension meta object, described below.
 * @returns {Object} an object with enable() and disable() methods
 */
function init(meta) {
  log(`Initializing ${meta.metadata.name}`);

  return new Extension();
}
